package main

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
)

var plateform string
var version string

func main() {
	plateform = "dev"
	version = "v0.0.1"

	fmt.Printf("Hello world, plateform %s and version %s\n", plateform, version)

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	if err := app.Listen(":3000"); err != nil {
		log.Fatal(err)
	}
}
